#!/bin/bash
echo "umounting" &> /home/centos/log
umount /dev/xvde &>> /home/centos/log
mkfs.ext4 -q /dev/xvde &>> /home/centos/log
mount /dev/xvde /media &>> /home/centos/log
echo "mounting" &>> /home/centos/log