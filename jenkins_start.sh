#!/bin/bash
echo "Update and install tools" &> /home/centos/log
yum update -y && yum -y install traceroute bind-utils net-tools wget unzip tcpdump screen nano mc vim  &>> /home/centos/log
# Add repo Docker
# yum install -y yum-utils
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo &>> /home/centos/log
#Install Docker CE
sudo yum makecache fast &>> /home/centos/log
sudo yum install docker-ce &>> /home/centos/log
sudo usermod -aG docker centos &>> /home/centos/log
#Install Docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose &>> /home/centos/log
sudo chmod +x /usr/local/bin/docker-compose &>> /home/centos/log 
sudo systemctl enable docker &>> /home/centos/log
sudo systemctl start docker &>> /home/centos/log