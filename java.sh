sudo -i 
echo "Installing version jdk-8u101-linux-x64"
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u101-b13/jdk-8u101-linux-x64.rpm" -O /opt/jdk-8u101-linux-x64.rpm
rpm -ivh /opt/jdk-8u101-linux-x64.rpm

alternatives --install /usr/bin/java java /usr/java/jdk1.8.0_101/bin/java 200000
alternatives --install /usr/bin/javaws javaws /usr/java/jdk1.8.0_101/bin/javaws 200000
alternatives --install /usr/bin/javac javac /usr/java/jdk1.8.0_101/bin/javac 200000
alternatives --install /usr/bin/jar jar /usr/java/jdk1.8.0_101/bin/jar 200000

echo "Install version jdk-7u60-linux-x64"
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u60-b19/jdk-7u60-linux-x64.rpm" -O /opt/jdk-7u60-linux-x64.rpm
rpm -ivh /opt/jdk-7u60-linux-x64.rpm

alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_60/bin/java 200000
alternatives --install /usr/bin/javaws javaws /usr/java/jdk1.7.0_60/bin/javaws 200000
alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_60/bin/javac 200000
alternatives --install /usr/bin/jar jar /usr/java/jdk1.7.0_60/bin/jar 200000