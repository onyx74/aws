#!/bin/bash
# set -e
echo "The script to run instance starts !"
echo "Hi $USER"

#Add new variables
KeyName=tmp
SGName=tmp-SG
VpcID=vpc-6071a207
DSG="VGladush_TMP_Secure_GROUP"
ImID=ami-d2c924b2
InT=m3.medium


# Creating a key
aws ec2 create-key-pair --key-name $KeyName --query 'KeyMaterial' --output text > /tmp/$KeyName.pem 2>/dev/null
chmod 600 /tmp/$KeyName.pem
echo "KeyName is [ $KeyName ] and created in [ /tmp/$KeyName.pem ]" | tee -a aws.log


# Create a SG for instance
SGID=$(aws ec2 create-security-group --group-name $SGName --description $DSG --vpc-id $VpcID | jq '.GroupId' | tr -d \") 2>/dev/null
echo SGID is [ $SGID ] | tee -a aws.log

#Add port to SG 0.0.0.0/0 for all ip
echo "Add port to SG 0.0.0.0/0 for all ip"
aws ec2 authorize-security-group-ingress --group-id $SGID --protocol tcp --port 22 --cidr 0.0.0.0/0 2>/dev/null
echo "Security group has been set up"

#Run-instance and get id
echo Run-instance and get id

FBDM=file://mapping.json 
FUD=file://start.sh

InstID=$(aws ec2 run-instances --image-id $ImID --count 1 --instance-type $InT --key-name $KeyName --security-group-ids $SGID --block-device-mappings $FBDM --user-data $FUD | jq '.Instances[0].InstanceId' | tr -d \")
echo InstanceId is [ $InstID ] | tee -a aws.log

# Get VolumeID
VolID=$(aws ec2 describe-instances  --filters "Name=instance-id,Values=$InstID" | jq '.Reservations[0].Instances[0].BlockDeviceMappings[0].Ebs.VolumeId' | tr -d \")
echo VolumeID is [ $VolID ] | tee -a aws.log

# Get IP .PublicIpAddress
PIP=$(aws ec2 describe-instances  --filters "Name=instance-id,Values=$InstID" | jq '.Reservations[0].Instances[0].PublicIpAddress' | tr -d \")
echo PublicIpAddress [ $PIP ] | tee -a aws.log

echo  "##############################################################"
echo  "# Run instance complete                                      "
echo  "# InstanceId - [ $InstID ]                                   "
echo  "# VolumeID   - [ $VolID ]                                    "
echo  "# IpAddress  - [ $PIP ]                                      "
echo  "For connect to instance use command: ssh -i /tmp/tmp.pm centos@$PIP"
echo  "##############################################################"


# Creating Snapshot
echo Creating Snapshot
SID=$(aws ec2 create-snapshot --volume-id vol-0fd40c576a3254c51 --description "TMP Snapshot" | jq '.SnapshotId' | tr -d \")
echo  "##############################################################"
echo Wait for the snapshot to be created!
echo  "##############################################################"
aws ec2 wait snapshot-completed --snapshot-id $SID

if [ $? -eq 0 ]; then
  echo SnapshotId [ $SID ] | tee -a aws.log
fi

# SNAP=file://snap.json
# SNAP="[{\"DeviceName\":\"/dev/xvda\",\"Ebs\":{\"SnapshotId\":\ $SID }}]"

#Create AMI
echo Creating AMI from SnapshotId
ImID2=$(aws ec2 register-image --name "AMI_VGtmp" --root-device-name /dev/xvda --block-device-mappings "[{\"DeviceName\":\"/dev/xvda\",\"Ebs\":{\"SnapshotId\":\"$SID\"}}]" --description "AMI for test" --architecture x86_64 --virtualization-type hvm | jq '.ImageId' | tr -d \")
echo AMI ID is [ $ImID2 ] | tee -a aws.log


#Run instance from snapshot
echo RUN Instance from AMI
InstIDSn=$(aws ec2 run-instances --image-id $ImID2 --count 1 --instance-type $InT --key-name $KeyName --security-group-ids $SGID --block-device-mappings "[{\"DeviceName\":\"/dev/xvda\",\"Ebs\":{\"SnapshotId\":\"$SID\"}}]" | jq '.Instances[0].InstanceId' | tr -d \")
echo Instance ID [ $InstIDSn ] | tee -a aws.log

# Get IP .PublicIpAddress
PIPSn=$(aws ec2 describe-instances  --filters "Name=instance-id,Values=$InstIDSn" | jq '.Reservations[0].Instances[0].PublicIpAddress' | tr -d \")
echo PublicIpAddress [ $PIPSn ] | tee -a aws.log


echo  "##############################################################"
echo  "# Creating Snapshot, AMI and Run instance complete           "
echo  "# SnapshotId - [ $SID ]                                      "
echo  "# AMI ID     - [ $ImID2 ]                                    "
echo  "# InstanceId - [ $InstIDSn ]                                 "
echo  "# IpAddress  - [ $PIPSn ]                                    "
echo  "For connect to instance use command: ssh -i /tmp/tmp.pm centos@$PIPSn"
echo  "##############################################################"

sleep 1

echo Log file has been created aws.log

echo Choose an action to continue!
PS3='Please enter your choice: '
options=("Terminate first Instance" "Terminate Instance from Snapshot" "Delete Snapshot" "Delete AMI" "Delete Key" "Delete Security Group" "Connect to 1 inst" "Connect to inst from snap" "Terminate ALL" "Exit")
select opt in "${options[@]}"
do
    case $opt in
        "Terminate first Instance")
            echo "Terminate first Instance"
            aws ec2 terminate-instances --instance-ids $InstID
            echo Instance - [$InstID] has been deleted 
            ;;
        "Terminate Instance from Snapshot")
            echo "Terminate Instance from Snapshot"
            aws ec2 terminate-instances --instance-ids $InstIDSn
            echo Instance - [$InstIDSn] has been deleted 
            ;;
        "Delete Snapshot")
            echo "Delete Snapshot"
            aws ec2 delete-snapshot --snapshot-id $SID
            echo Snapshot - [$SID] has been deleted
            ;;
        "Delete AMI")
            echo "Delete AMI"
            aws ec2 deregister-image --image-id $ImID2
            echo AMI - [$ImID2] has been deleted
            ;;
        "Delete Key")
            echo "Delete Key"
            rm /tmp/tmp.pem
            aws ec2 delete-key-pair --key-name $KeyName
            echo key - [$KeyName] has been deleted
            ;;
        "Delete Security Group")
            echo "Delete Security Group"
            aws ec2 delete-security-group --group-name tmp-SG
            echo SecurityGroup - [$SGName] has been deleted
            ;;
        "Connect to 1 inst")
            echo "Connect to 1 inst"
            ssh -i /tmp/tmp.pem centos@$PIP
            ;;
        "Connect to inst from snap")
            echo "Connect to inst from snap "
            ssh -i /tmp/tmp.pem centos@$PIPSn
            ;;
        "Terminate ALL")
            echo "Terminate first Instance"
            aws ec2 terminate-instances --instance-ids $InstID
            echo Instance - [$InstID] has been deleted
            echo "Terminate Instance from Snapshot"
            aws ec2 terminate-instances --instance-ids $InstIDSn
            echo Instance - [$InstIDSn] has been deleted
            echo "Delete AMI"
            aws ec2 deregister-image --image-id $ImID2
            echo AMI - [$ImID2] has been deleted
            echo "Delete Key"
            rm /tmp/tmp.pem
            aws ec2 delete-key-pair --key-name $KeyName
            echo key - [$KeyName] has been deleted            
            echo "Delete Securiry Group"
            aws ec2 delete-security-group --group-name $SGName
            echo SecurityGroup - [$SGName] has been deleted
            ;;
        "Exit")
            break
            ;;
        *) echo invalid option;;
    esac
done



